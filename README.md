# HBOND

Required **CMAKE ≥ 3.16.3** and **Gromacs ≥ 2024**.

To build run:
```
mkdir build
cd build/
cmake ../gmx-dssp
make
```

In order to analyze a mesh of hydrogen bonds in the system (both in a static structure and in the trajectory), containing objects that potentially form a hydrogen bonds (such as organic polymers and/or water molecules), you can use the **HBOND** program, which uses calculation of distances and angles between atoms as a geometric criterion for the formation of hydrogen bonds. For the work of the program, it is necessary to submit to the input (select from the list) of 2 groups of atoms from the topology of the structure, which must be either absolutely coinciding or completely different in the atoms contained in them. The program will search for potentially existing hydrogen bonds between these two groups (or inside the same group, if only one was indicated). By default, after the analysis of the system, the program will present an index map containing acceptors from each group, donors indices (and hydrogen atoms associated with them) from each group and donors and acceptors indices (and index of the hydrogen atom associated with them, if hydrogens were not merged) of all detected hydrogen bonds. Nitrogen atoms (together with oxygen atoms) are considered potential acceptors of hydrogen bond, but there is a special option that can be applied if only oxygen atoms must be considered potential acceptors. The **HBOND** supports the possibility of issuing a map of hydrogen bonds both for the entire structure as a whole and for each frame in the trajectory. There is an option to merge hydrogen bonds with identical donors and acceptors, but differing hydrogen atoms. A statistical analysis of the data obtained is also available. For example, you can, in a wishes, receive at the output a schedule for distributions of distances and/or angles between donors and acceptors of hydrogen bonds. You can also get a schedule for the number of hydrogen bonds and/or donors and acceptors on each analyzed temporary segment of the structure.
